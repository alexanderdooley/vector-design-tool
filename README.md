# Vector Design Tool

The goal of this project was to create a piece of software in Java to edit and create files written in a proprietary design language. As a group we developed a GUI application that allowed the user to create vector text files using an interface similar to MS Paint

![Example Import of .vec File](img/CAB302_VECPaint_Eg.PNG)

### Running VEC Paint

The published build can be found in the folder, bin. Example .vec files are located in the tests 
folder.

### Authors

* **Alex Dooley**
* **Nicholas Beaumont**
* **Tolga Pasin**
* **Jens Xue**